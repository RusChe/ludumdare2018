VAR drink_name = "DRINK"
VAR angered = false
"Where is my {drink_name}{~, I'm still waiting?|? This is so boring!}"
*   "Coming right away"
    "{~Just be faster, oh my gosh|Say please next time}!" -> END
*   "Go somewhere else if you can't wait!"
    ~ angered = true
    "What? Don't be rude." -> END