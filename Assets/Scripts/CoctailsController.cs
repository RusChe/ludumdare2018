﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoctailsController : MonoBehaviour
{

	private static CoctailsController instance;
	public static CoctailsController I
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<CoctailsController>();
			return instance;
		}
	}
	
	public Color[] ingredientsColor;
	public List<string> drinkNames;
	public List<DrinkNote> drinkNotes;
	
	[HideInInspector]
	public string[] currentDrinkNames;
	
	// TODO: rename placeholders
	public enum DrinkType
	{
		Drink1,
		Drink2,
		Drink3,
		Drink4,
		Drink5
	}

	public enum IngredientType
	{
		Red,
		Yellow,
		Blue,
		Green,
		xxx
	}

	[System.Serializable]
	public class Drink
	{
		public DrinkType drinkType;
		public List<IngredientType> ingredients;
	}

	[HideInInspector]
	public List<Drink> PossibleDrinks;

	private void Awake()
	{
		NewGame();
	}

	public void NewGame()
	{
		drinkNames.Shuffle();
		currentDrinkNames = drinkNames.GetRange(0, 5).ToArray();
		// generate drinks
		PossibleDrinks = new List<Drink>();
		for (int i = 0; i < 5; i++)
		{
			var drink = new Drink { drinkType = (DrinkType) i, ingredients =  new List<IngredientType>()};
			for (int j = 0; j < 4; j++)
			{
				// only normal ingredients, no XXX
				drink.ingredients.Add((IngredientType)Random.Range(0, 4));
			}
			PossibleDrinks.Add(drink);
		}
		
		// refresh notes
		for (int i = 0; i < drinkNotes.Count; i++)
		{
			drinkNotes[i].Refresh(PossibleDrinks[i]);
		}
	}
}
