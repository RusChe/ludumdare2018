﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Drinker : MonoBehaviour {

	public enum State
	{
		Ordering,
		WaitingForDrink,
		Angry,
		Finished,
	}

	public int DrinkerID;

	public bool isInitialized;
	public TextAsset openingInkAsset;
	public TextAsset waitingInkAsset;
	public TextAsset angryInkAsset;

	private Transform doorTransform;

	public State CurrentState = State.Ordering;
	
	public bool IsUndercover;

	public CoctailsController.DrinkType WantedDrink;

	public float speed = 5f;
	
	private Random random = new Random();
	private CharacterAnimator animator;

	private Vector3 startPosition;
	private Quaternion startRotation;

	public bool IsSelected;
	public bool IsWalkingHome;
	public bool IsWalkingBack;

	public bool HasAlcohol;
	public bool IsOkDrink;

	private void Awake()
	{
		animator = GetComponent<CharacterAnimator>();
		doorTransform = GameObject.Find("doorToHome").transform;
		startPosition = transform.position;
		startRotation = transform.rotation;
	}

	public void Initialize(bool isCop = false)
	{
		CurrentState = State.Ordering;
		IsUndercover = isCop;
		
		Array values = Enum.GetValues(typeof(CoctailsController.DrinkType));
		WantedDrink = (CoctailsController.DrinkType)values.GetValue(random.Next(values.Length));
	}

	public void DoAngry()
	{
		CurrentState = State.Angry;
		animator.Angry();
		StartCoroutine(StartWalkingHome());
	}

	public void DoHappy()
	{
		CurrentState = State.Finished;
		StartCoroutine(StartWalkingHome());
	}

	private IEnumerator StartWalkingHome()
	{
		yield return new WaitForSecondsRealtime(3f);
		IsWalkingHome = true;
	}

	public void NodHead(bool confirm)
	{
		if (confirm)
			animator.Confirm();
		else
			animator.Dismiss();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (IsWalkingHome && other.gameObject.layer == LayerMask.NameToLayer("Door"))
		{
			StartCoroutine(WalkBackHome());
		}
	}

	private IEnumerator WalkBackHome()
	{
		IsWalkingHome = false;

		transform.position = -1000f * Vector3.one;
		yield return new WaitForSecondsRealtime(4f);

		transform.position = doorTransform.transform.position;
		IsWalkingBack = true;
		CurrentState = State.Ordering;
		isInitialized = false;
		IsUndercover = UnityEngine.Random.value > 0.5f;
		IsOkDrink = false;
		HasAlcohol = false;
	}

	public void GiveDrink(Inventory_drink drink, Action callback)
	{
		CurrentState = State.Finished;
		drink.GivenToCustomer = true;

		var wantedDrink = CoctailsController.I.PossibleDrinks.Find(d => d.drinkType == WantedDrink);
		IsOkDrink = true;
		HasAlcohol = false;
		var alchocolCanReplaceUpTo = 1;
		for (int i = 0; i < wantedDrink.ingredients.Count; i++)
		{
			var ingredient = (CoctailsController.IngredientType) drink.Ingredients[i];
			if (wantedDrink.ingredients[i] != ingredient)
			{
				var isAlchocol = ingredient == CoctailsController.IngredientType.xxx;
				if (isAlchocol)
				{
					HasAlcohol = true;
				}
				if (isAlchocol && alchocolCanReplaceUpTo > 0)
				{
					alchocolCanReplaceUpTo--;
				}
				else
				{
					IsOkDrink = false;
					break;
				}
			}
		}

		StartCoroutine(DrinkCoctail(callback));
	}

	private IEnumerator DrinkCoctail(Action callback)
	{
		animator.Drink();
		yield return new WaitForSecondsRealtime(3f);
		callback();
	}

	private void Update()
	{
		if (!Application.isPlaying)
			return;

		if (IsWalkingBack)
		{
			animator.SetWalking(true);
			transform.position = Vector3.MoveTowards(transform.position, startPosition, speed * Time.deltaTime);
			transform.LookAt(startPosition);

			if (Vector3.Distance(transform.position, startPosition) < 0.1f)
			{
				transform.position = startPosition;
				transform.rotation = startRotation;
				IsWalkingBack = false;
			}
		}
		else if (!IsSelected && IsWalkingHome)
		{
			// go towards door
			animator.SetWalking(true);
			
			transform.position = Vector3.MoveTowards(transform.position, doorTransform.position, speed * Time.deltaTime);
			transform.LookAt(doorTransform);
		}
		else
		{
			animator.SetWalking(false);
		}
	}
}
