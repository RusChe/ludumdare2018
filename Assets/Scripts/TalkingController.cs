﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class TalkingController : MonoBehaviour
{

	public Camera cam;
	public LayerMask talkingLayerMask;
	
	public CinemachineBrain cinemachineBrain;
	public FirstPersonController fpsController;

	public CinemachineVirtualCamera idleCamera;
	public CinemachineVirtualCamera talkingCamera;
	public CinemachineVirtualCamera notesCamera;
	public CinemachineVirtualCamera shakingCamera;

	private CinemachineVirtualCamera currentlyActiveCamera;

	private CursorController cursor;
	private DialogController dialogController;
	private InventoryController inventoryController;

	private Drinker talkingToDrinker;
	
	public bool IsTalking
	{
		get { return currentlyActiveCamera == talkingCamera; }
	}

	public bool IsCheckingNotes
	{
		get { return currentlyActiveCamera == notesCamera; }
	}

	private void Start()
	{
		inventoryController = FindObjectOfType<InventoryController>();
		cursor = FindObjectOfType<CursorController>();
		dialogController = FindObjectOfType<DialogController>();
		ActivateTalking(null);
	}

	private IEnumerator ToggleShakingCamera()
	{
		if (currentlyActiveCamera == shakingCamera)
		{
			ActivateCamera(idleCamera);
			
			yield return new WaitForSecondsRealtime(0.6f);
			
			fpsController.enabled = true;
			cinemachineBrain.enabled = false;
		}
		else
		{
			// TODO: this is not working properly, idle is not looking where fps stopped
			//idleCamera.transform.rotation = fpsController.transform.localRotation;
			fpsController.enabled = false;
			cinemachineBrain.enabled = true;
			
			yield return new WaitForSecondsRealtime(0.1f);
			
			ActivateCamera(shakingCamera);
		}
	}

	public void ShowShakingCamera()
	{
		cursor.SetCursor(false);
		StartCoroutine(ToggleShakingCamera());
	}

	public void HideShakingCamera()
	{
		cursor.SetCursor(true);
		StartCoroutine(ToggleShakingCamera());
	}

	public void ActivateTalking(Transform talkTo, bool isNote = false)
	{
		cursor.SetCursor(talkTo == null);
		StartCoroutine(StartTalkingFlow(talkTo, isNote));

		if (isNote)
			return;
		
		if (talkingToDrinker != null)
		{
			talkingToDrinker.IsSelected = false;
		}

		if (talkTo != null)
		{
			talkingToDrinker = talkTo.GetComponent<Drinker>();
			talkingToDrinker.IsSelected = true;
		}
		else
		{
			talkingToDrinker = null;
		}
	}

	private IEnumerator StartTalkingFlow(Transform talkTo, bool isNote = false)
	{
		if (talkTo != null)
		{
			// TODO: this is not working properly, idle is not looking where fps stopped
			//idleCamera.transform.rotation = fpsController.transform.localRotation;
			fpsController.enabled = false;
			cinemachineBrain.enabled = true;
			
			yield return new WaitForSecondsRealtime(0.1f);

			var zoomCamera = isNote ? notesCamera : talkingCamera;
			zoomCamera.LookAt = talkTo;
			zoomCamera.Follow = talkTo;
			ActivateCamera(zoomCamera);
		}
		else
		{
			ActivateCamera(idleCamera);
			
			yield return new WaitForSecondsRealtime(0.6f);
			
			fpsController.enabled = true;
			cinemachineBrain.enabled = false;
		}
	}

	public void ActivateCamera(CinemachineVirtualCamera activeCam)
	{
		idleCamera.Priority = activeCam == idleCamera ? 100 : 1;
		talkingCamera.Priority = activeCam == talkingCamera ? 100 : 1;
		notesCamera.Priority = activeCam == notesCamera ? 100 : 1;
		shakingCamera.Priority = activeCam == shakingCamera ? 100 : 1;
		currentlyActiveCamera = activeCam;
	}

	private void Update()
	{
		if (GameManager.I.IsTutorial)
			return;
		
		if (Input.GetMouseButtonDown(0))
		{
			if (IsTalking)
			{
				if (dialogController.CanBeClosed)
				{
					dialogController.DeactivateDialog();
					inventoryController.EnableInvetorty();
					ActivateTalking(null);
				}
			}
			else if (IsCheckingNotes)
			{
				inventoryController.EnableInvetorty();
				ActivateTalking(null);
			}
			else
			{
				Ray ray = cam.ScreenPointToRay(new Vector2(Screen.width / 2f, Screen.height / 2f));
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, 100f, talkingLayerMask))
				{
					if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Chars"))
					{
						var drinker = hit.transform.gameObject.GetComponent<Drinker>();
						if (!drinker.IsWalkingBack && drinker.CurrentState != Drinker.State.Finished)
						{
							dialogController.ActivateDialog(drinker);
							inventoryController.DisableInvetory();
							ActivateTalking(hit.transform);
						}
					}
					else if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Notes"))
					{
						inventoryController.DisableInvetory();
						ActivateTalking(hit.transform, true);
					}
					else if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Coaster"))
					{
						var selectedObject = inventoryController.GetSelectedObject();
						if (selectedObject != null)
						{
							var coctail = selectedObject.GetComponent<Inventory_drink>();
							if (coctail != null)
							{
								var index = hit.transform.GetSiblingIndex();
								var drinker = DrinkerManager.I.drinkers[index];
								if (drinker.CurrentState == Drinker.State.WaitingForDrink)
								{
									inventoryController.PutDownSelectedObject(hit.point);
									
									drinker.GiveDrink(coctail, () =>
									{
										Destroy(coctail.gameObject);
										dialogController.ActivateDialog(drinker);
									});
									ActivateTalking(drinker.transform);
									
									inventoryController.DisableInvetory();
								}
								else if (drinker.CurrentState == Drinker.State.Ordering)
								{
									InfoController.I.ShowMessage("Ask for order first!");
								}
								else if (drinker.CurrentState == Drinker.State.Angry)
								{
									InfoController.I.ShowMessage("Can't give drink to angry customer!");
								}
								else if (drinker.CurrentState == Drinker.State.Finished)
								{
									InfoController.I.ShowMessage("Empty place!");
								}
							}
						}
						else
						{
							//InfoController.I.ShowMessage("Put drink on the coaster!");
						}
					}
				}
			}
		}
	}
}
