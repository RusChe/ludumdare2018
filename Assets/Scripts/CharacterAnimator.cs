﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{

	public float cycleOffsetLimit = 10f;
	
	private Animator charAnimator;

	void Start()
	{
		charAnimator = GetComponent<Animator>();
		charAnimator.Play("idle", -1, Random.value * cycleOffsetLimit);
	}

	public void Angry()
	{
		charAnimator.SetTrigger("explain");
	}

	public void Confirm()
	{
		charAnimator.SetTrigger("confirm");
	}

	public void Dismiss()
	{
		charAnimator.SetTrigger("dismiss");
	}

	public void SetWalking(bool isWalking)
	{
		charAnimator.SetBool("walking", isWalking);
	}

	public void Drink()
	{
		charAnimator.SetTrigger("drink");
	}
}
