﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrinkNote : MonoBehaviour
{

	public Text drinkName;

	public Renderer[] ingredientRenderers;

	public void Refresh(CoctailsController.Drink drink)
	{
		drinkName.text = CoctailsController.I.currentDrinkNames[(int)drink.drinkType];
		for (int i = 0; i < ingredientRenderers.Length; i++)
		{
			if (i < drink.ingredients.Count)
			{
				ingredientRenderers[i].gameObject.SetActive(true);
				ingredientRenderers[i].material.color = CoctailsController.I.ingredientsColor[(int) drink.ingredients[i]];
			}
			else
			{
				ingredientRenderers[i].gameObject.SetActive(false);
			}
		}
	}
	
}
