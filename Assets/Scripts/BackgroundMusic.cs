﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{

	public List<AudioClip> backgroundMusicClips;

	private AudioSource audioSource;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Update()
	{
		audioSource.UnPause();
		if (!audioSource.isPlaying)
		{
			audioSource.clip = backgroundMusicClips.GetRandom();
			audioSource.Play();
		}
	}
}
