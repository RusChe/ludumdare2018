﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory_drink : MonoBehaviour {

    public List<int> Ingredients;

    public bool GivenToCustomer;

    public void FillIngredients(List<int> ingred)
    {
        Ingredients = ingred;
    }
}
