﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum Items
{
    Bottle1 = 0, Bottle2, Bottle3, Bottle4
}
public class InventoryController : MonoBehaviour
{
    public Camera cam;
    public float rayMaxDistance;
    public int inventorSize;
    public Transform inventroryBar;
    public GameObject hintObject;
    public GameObject itemPrefab;
    public GameObject frame;

    public float tileOffset = 70;
    public bool placeing = false;
    public LayerMask layerMask;
    public LayerMask layerMaskBar;
    float currnetPostion = 0;
    bool invetoryActive = true;
    int ObjectToPlace = -1;
    public bool shaking = false;
    Shaker shaker;
    public GameObject inventory_Drink;
    public GameObject coctailGlass;


    public Texture[] rawImage;

    List<GameObject> Inventory = new List<GameObject>(); // used for ui elemnts
    List<GameObject> CollectedObjects = new List<GameObject>(); // used for 3d models

    public GameObject GetSelectedObject()
    {
        if (!placeing)
            return null;

        return CollectedObjects[ObjectToPlace];
    }


    // Use this for initialization
    void Start()
    {
        frame.SetActive(false);
        shaker = FindObjectOfType<Shaker>();
    }

    public void EnableInvetorty()
    {
        inventroryBar.gameObject.SetActive(true);
        invetoryActive = true;

    }
    public void DisableInvetory()
    {
        inventroryBar.gameObject.SetActive(false);
        invetoryActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.I.IsTutorial)
            return;
        
        if (invetoryActive)
        {

            #region Global Ray Cast
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2f, Screen.height / 2f));

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.layer == layerMaskBar)
                    print("Bar tabelHit");
            }
            #endregion

            #region Changing mode grab/place
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (placeing && ObjectToPlace == 0)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 0;
                    frame.transform.localPosition = new Vector3(1 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();
                }

            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (placeing && ObjectToPlace == 1)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 1;
                    frame.transform.localPosition = new Vector3(2 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }

            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (placeing && ObjectToPlace == 2)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 2;
                    frame.transform.localPosition = new Vector3(3 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }

            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (placeing && ObjectToPlace == 3)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 3;
                    frame.transform.localPosition = new Vector3(4 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                if (placeing && ObjectToPlace == 4)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 4;
                    frame.transform.localPosition = new Vector3(5 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }

            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                if (placeing  && ObjectToPlace == 5)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 5;
                    frame.transform.localPosition = new Vector3(6 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                if (placeing && ObjectToPlace == 6)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 6;
                    frame.transform.localPosition = new Vector3(7 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                if (placeing && ObjectToPlace == 7)
                {
                    placeing = false;
                    frame.SetActive(false);
                    AddIngrediand();
                }
                else
                {
                    placeing = true;
                    ObjectToPlace = 7;
                    frame.transform.localPosition = new Vector3(8 * tileOffset, 0.7f, 0);
                    frame.SetActive(true);
                    AddIngrediand();


                }

            }
            #endregion

            #region Shaker mode ON/OFF
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (shaking)
                {
                    if (!shaker.isEmpty())      //Shaking done, return final coctail
                    {
                        var coctailData = shaker.GetCoctail();
                        AddItem(inventory_Drink, coctailData);                  
                    }
                    shaking = false;
                    shaker.ShakerModeActive = false;
                    // frame.SetActive(false);
                    hintObject.SetActive(false);
                    FindObjectOfType<TalkingController>().HideShakingCamera();
                }
                else
                {
                    //shaking = true;
                    // shaker.ShakerModeActive = true;
                    //FindObjectOfType<TalkingController>().ShowShakingCamera();
                }
            }
            if (Input.GetMouseButtonDown(0))      
            {
                if (!shaking)
                {
                    // Grabing or placing
                    if (!placeing)
                    {
                        RaycastHit target;

                        if (Physics.Raycast(ray, out target, rayMaxDistance, layerMask))
                        {
                            if (target.transform.gameObject.CompareTag("shaker"))
                            {
                                shaking = true;
                                shaker.ShakerModeActive = true;
                                FindObjectOfType<TalkingController>().ShowShakingCamera();
                                hintObject.SetActive(true);
                            }
                            else
                            {
                                var coctail = target.transform.gameObject.GetComponent<Inventory_drink>();
                                if (coctail == null || !coctail.GivenToCustomer)
                                {
                                    AddItem(target);
                                    print("Grabed :" + target.transform.name); 
                                }
                            }
                        }
                    }
                    else
                    {
                        RaycastHit target;
                        if (Physics.Raycast(ray, out target, rayMaxDistance, layerMask))
                        {
                            if (target.transform.gameObject.CompareTag("shaker"))
                            {
                                placeing = false;
                                frame.SetActive(false);
                                
                                shaking = true;
                                shaker.ShakerModeActive = true;
                                FindObjectOfType<TalkingController>().ShowShakingCamera();
                                hintObject.SetActive(true);
                            }
                        }
                        else if (Physics.Raycast(ray, out target, rayMaxDistance, layerMaskBar))
                        {
                            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Surfice"))
                            {
                                PutDownSelectedObject(target.point);
                            }
                        }
                        
                    }
                }
                else
                {
                    hintObject.SetActive(false);
                    FindObjectOfType<TalkingController>().HideShakingCamera();
                    shaking = false;
                    shaker.ShakerModeActive = false;
                    frame.SetActive(false);
                    placeing = false;
                }
            }   
            #endregion
            
          
            // Reset shaker
            if(Input.GetKeyDown(KeyCode.R)&&shaking)
            {
                shaker.ResetList();
            }
        }

    }

    public void PutDownSelectedObject(Vector3 target_position)
    {
        RemoveItem(ObjectToPlace, target_position);
        print(target_position);
        frame.SetActive(false);
        placeing = false;
    }

    void AddIngrediand()
    {
        if (shaking)
        {
            AddElementToShaker();
            frame.SetActive(true);
            StartCoroutine(DisableFrame());
        }
    }
    IEnumerator DisableFrame()
    {
        yield return new WaitForSeconds(0.2f);
        frame.SetActive(false);
    }
    void AddItem(RaycastHit hit)
    {
        // Use hit to detect objcet tag and set image for it
        if (Inventory.Count < inventorSize)
        {
            GameObject item;
            item = Instantiate(itemPrefab) as GameObject;
            item.transform.SetParent(inventroryBar, true);
            item.transform.localScale = Vector3.one;
            //  item.transform.parent = inventroryBar;

            // UI elemets
            currnetPostion += tileOffset;
            item.transform.localPosition = new Vector3(currnetPostion, 0.7f, 0);

            string itemName = hit.transform.gameObject.name;
            RawImage itemImg = item.GetComponent<RawImage>();
            print("Item name:" + itemName);

            // Setting image to tile

            switch (itemName)
            {
                case "Bottle1":
                    item.GetComponent<RawImage>().texture = rawImage[0];
                    break;
                case "Bottle2":
                    item.GetComponent<RawImage>().texture = rawImage[1];
                    break;
                case "Bottle3":
                    item.GetComponent<RawImage>().texture = rawImage[2];
                    break;
                case "Bottle4":
                    item.GetComponent<RawImage>().texture = rawImage[3];
                    break;
                case "Bottle0":
                    item.GetComponent<RawImage>().texture = rawImage[4];
                    break;
                default:
                    break;
            }

            if (itemName.Contains("coctailGlass"))
            {
                item.GetComponent<RawImage>().texture = rawImage[5]; 
            }
            Inventory.Add(item);
            GameObject model = hit.transform.gameObject;
            model.SetActive(false);
            CollectedObjects.Add(model);


        }
        else
        {
            print("Inventory full");
            // Implement user interface for this message

        }

    }

    // Overload used for prepared coctail befor serving
    void AddItem(GameObject prefab, List<int> ingred)
    {
        // Use hit to detect objcet tag and set image for it
        if (Inventory.Count < inventorSize)
        {
            GameObject item;
            item = Instantiate(prefab) as GameObject;
            item.transform.SetParent(inventroryBar, true);
            item.transform.localScale = Vector3.one;

            // UI elemets
            currnetPostion += tileOffset;
            item.transform.localPosition = new Vector3(currnetPostion, 0.7f, 0);

            string itemName = "Coctail";
            RawImage itemImg = item.GetComponent<RawImage>();
            print("Item name:" + itemName);

            // Setting image to tile
            item.GetComponent<RawImage>().texture = rawImage[5];

            Inventory.Add(item);
            GameObject model = Instantiate(coctailGlass);
            model.SetActive(false);
            CollectedObjects.Add(model);
            var glass = model.GetComponent<Inventory_drink>();
            glass.FillIngredients(ingred);

        }
        else
        {
            print("Inventory full");
            InfoController.I.ShowMessage("Inventory full");
            // Implement user interface for this message

        }

    }


    void RemoveItem(int objectToPlace, Vector3 transform)
    {
        if (Inventory.Count != 0 || Inventory.Count > ObjectToPlace)
        {
            Inventory[objectToPlace].SetActive(false);
            moveTilesLeft(ObjectToPlace);

            Destroy(Inventory[objectToPlace]);
            Inventory.RemoveAt(objectToPlace);
            currnetPostion -= tileOffset;
            // Call move left method
            CollectedObjects[objectToPlace].SetActive(true);
            CollectedObjects[objectToPlace].transform.position = new Vector3(transform.x, transform.y + 0.09f, transform.z); // Set target positin of raycast

            CollectedObjects.RemoveAt(objectToPlace);
        }
        else
        {
            print("Invetory empty");
            // Implement user interface for this message
        }
    }

    void moveTilesLeft(int objectToPlace)
    {
        if (Inventory.Count > objectToPlace)
        {
            for (int i = objectToPlace; i < Inventory.Count; i++)
            {
                Inventory[i].transform.localPosition = new Vector3(Inventory[i].transform.localPosition.x - tileOffset, Inventory[i].transform.localPosition.y, Inventory[i].transform.localPosition.z);
            }
            print("position has changed");
        }

    }

    void AddElementToShaker()
    {

        GameObject go = null;
        int itemToAdd = -1; // This represents type of drink

        if (ObjectToPlace < CollectedObjects.Count)
            go = CollectedObjects[ObjectToPlace];  // Object to place is avtiv object in inventory
        else
        {
            print("Invetory is empty.");
            InfoController.I.ShowMessage("Invetory is empty.");
        }

        frame.SetActive(false); // Frame over active tile


        if (go.name == "Bottle1")
        {
            itemToAdd = 0;
        }
        else if (go.name == "Bottle2")
        {
            itemToAdd = 1;
        }
        else if (go.name == "Bottle3")
        {
            itemToAdd = 2;
        }
        else if (go.name == "Bottle4")
        {
            itemToAdd = 3;
        }
        else if (go.name == "Bottle0")
        {
            itemToAdd = 4;
        }

        // Working with shaker type

        shaker.AddSupstance(itemToAdd);

    }
}


