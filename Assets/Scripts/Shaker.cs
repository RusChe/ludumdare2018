﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Here enum for suspstances
enum Substance
{
    red, blue
}
public class Shaker : MonoBehaviour
{

    bool shakerModeActive;
    public List<int> substances = new List<int>();
    public List<GameObject> Plates;
    public List<Material> material;

    int counter = 0;
    public bool isEmpty()
    {
        return substances.Count == 0;
    }

    public bool ShakerModeActive
    {
        set { shakerModeActive = value; }
    }           // Should be used for shaker root animation

    public void ResetList()
    {
        substances.Clear();
        counter = 0;
        for(int i=0;i<Plates.Count;i++)
            Plates[i].GetComponent<Renderer>().material = material[5];
    }
    public void AddSupstance(int substance)     // int is type of drink
    {
        if (counter < Plates.Count)
        {
            substances.Add(substance);
            if (substance >= 0)
            {
                Plates[counter].GetComponent<Renderer>().material = material[substance];
                counter++;
            }

            else
            {
                print("You can not mix coctail with other drinks !");
                InfoController.I.ShowMessage("You can not mix coctail with other drinks !");
            }

        }
        else
        {
            print("Only 4 elemetns are allowed");
            InfoController.I.ShowMessage("Only 4 elemetns are allowed");
        }
            // Call UI warrning
    }
    public List<int> GetCoctail()
    {
        List<int> Temp = new List<int>(substances);
        substances.Clear();
        ResetList();
        return Temp;
    }
}
