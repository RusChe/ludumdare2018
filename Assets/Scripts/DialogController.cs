﻿using System.Collections;
using System.Collections.Generic;
using Ink.Runtime;
using UnityEngine;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{

	public CanvasGroup dialogPanel;
	public Text replayLineText;
	public Text talkLineText;
	public Button[] choiceLineButtons;
	public Text[] choiceLineTexts;
	public Text responseLineText;
	public Color preSelectColor;
	public Color selectColor;

	public CanvasGroup optionsPanel;

	private TalkingController talkingController;
	
	private Story inkStory;
	
	// Set this file to your compiled json asset
	public List<TextAsset> openingNormalInkAssets;
	public List<TextAsset> openingUndercoverInkAssets;
	public List<TextAsset> waitingForDrinkInkAssets;
	public List<TextAsset> openingAngryInkAssets;
	public List<TextAsset> greatJobInkAssets;
	public List<TextAsset> goodJobInkAssets;
	public List<TextAsset> goodJobCopInkAssets;
	public List<TextAsset> badJobInkAssets;
	public List<TextAsset> bustedInkAssets;
	

	private bool isActive = false;
	private bool canChooseOptions;

	private bool angered;
	private bool orderedDrink;
	private bool busted;
	private bool isHappy;
	private Drinker currentDrinker;

	public bool CanBeClosed;

	private void Awake()
	{
		dialogPanel.alpha = 0f;
		talkingController = FindObjectOfType<TalkingController>();
	}

	public void ActivateDialog(Drinker drinker)
	{
		currentDrinker = drinker;
		CanBeClosed = false;
		isActive = true;
		ResetOptions();

		// todo: should be in drinker
		if (!drinker.isInitialized)
		{
			if (drinker.IsUndercover)
				drinker.openingInkAsset = openingUndercoverInkAssets.GetRandom();
			else
				drinker.openingInkAsset = openingNormalInkAssets.GetRandom();
			drinker.angryInkAsset = openingAngryInkAssets.GetRandom();
			drinker.waitingInkAsset = waitingForDrinkInkAssets.GetRandom();
			drinker.isInitialized = true;
		}

		isHappy = false;
		
		// load it depending on dialog id
		var asset = drinker.openingInkAsset;
		if (drinker.CurrentState == Drinker.State.Ordering)
		{
			asset = drinker.openingInkAsset;
		}
		else if (drinker.CurrentState == Drinker.State.WaitingForDrink)
		{
			asset = drinker.waitingInkAsset;
		}
		else if (drinker.CurrentState == Drinker.State.Angry)
		{
			asset = drinker.angryInkAsset;
		}
		else if (drinker.CurrentState == Drinker.State.Finished)
		{
			if (drinker.IsUndercover)
			{
				if (drinker.HasAlcohol)
				{
					asset = bustedInkAssets.GetRandom();
					busted = true;
				}
				else if (drinker.IsOkDrink)
				{
					isHappy = true;
					asset = goodJobCopInkAssets.GetRandom();
				}
				else
				{
					angered = true;
					asset = badJobInkAssets.GetRandom();
				}
			}
			else
			{
				if (drinker.IsOkDrink)
				{
					if (drinker.HasAlcohol)
					{
						isHappy = true;
						asset = greatJobInkAssets.GetRandom();
					}
					else
					{
						isHappy = true;
						asset = goodJobInkAssets.GetRandom();
					}
				}
				else
				{
					angered = true;
					asset = badJobInkAssets.GetRandom();
				}
			}
		}

		inkStory = new Story(asset.text);
		if (inkStory.variablesState.HasVariable("drink_name"))
		{
			inkStory.variablesState["drink_name"] = CoctailsController.I.currentDrinkNames[(int)drinker.WantedDrink];
		}
		StartCoroutine(ContinueStory(true));
		
		LeanTween.alphaCanvas(dialogPanel, 1f, 0.4f);
	}

	public void HideDialog()
	{
		dialogPanel.alpha = 0f;
	}

	public void DeactivateDialog()
	{
		isActive = false;
		LeanTween.alphaCanvas(dialogPanel, 0f, 0.4f);
		if (angered)
		{
			GameManager.I.DecreaseReputation(1);
		}
		else if (isHappy)
		{
			if (currentDrinker.HasAlcohol)
			{
				GameManager.I.IncreaseMoney(Random.Range(20, 30));
			}
			else
			{
				GameManager.I.IncreaseMoney(Random.Range(4, 8));
			}
		}
	}

	private void Update()
	{
		if (GameManager.I.IsTutorial)
			return;
		
		if (!canChooseOptions)
			return;
		
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			PreselectButton(0);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			PreselectButton(1);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			PreselectButton(2);
		}
		else
		{
			if (Input.GetKeyUp(KeyCode.Alpha1))
			{
				StartCoroutine(SelectButton(0));
			}
			else if (Input.GetKeyUp(KeyCode.Alpha2))
			{
				StartCoroutine(SelectButton(1));
			}
			else if (Input.GetKeyUp(KeyCode.Alpha3))
			{
				StartCoroutine(SelectButton(2));
			}
		}
	}

	private void PreselectButton(int index)
	{
		for (int i = 0; i < choiceLineButtons.Length; i++)
		{
			var image = choiceLineButtons[i].GetComponent<RectTransform>();
			LeanTween.color(image, index == i ? preSelectColor : Color.white, 0.4f);
		}
	}

	private void ResetOptions()
	{
		for (int i = 0; i < choiceLineButtons.Length; i++)
		{
			var image = choiceLineButtons[i].GetComponent<Image>();
			image.color = Color.white;
		}
	}

	private IEnumerator SelectButton(int index)
	{
		if (index <0 || index >= inkStory.currentChoices.Count || !canChooseOptions)
			yield break;
		
		canChooseOptions = false;
		for (int i = 0; i < choiceLineButtons.Length; i++)
		{
			var image = choiceLineButtons[i].GetComponent<RectTransform>();
			LeanTween.color(image, index == i ? selectColor : Color.white, 0.1f);
		}
		
		yield return new WaitForSecondsRealtime(0.4f);

		LeanTween.alphaCanvas(optionsPanel, 0f, 0.3f);
		LeanTween.alpha(talkLineText.rectTransform, 0f, 0.3f);
		
		inkStory.ChooseChoiceIndex(index);
		
		yield return ContinueStory();
	}

	private IEnumerator ShowTitleText(bool isStart)
	{
		LeanTween.alpha(talkLineText.rectTransform, 0f, 0.4f);
		talkLineText.text = "";

		replayLineText.text = isStart ? "" : inkStory.Continue().TrimEnd();
		replayLineText.gameObject.SetActive(!isStart);
		
		if (!inkStory.canContinue)
		{
			talkLineText.text = "<SILENT>";
			LeanTween.alpha(talkLineText.rectTransform, 0f, 0.4f);
			yield break;
		}

		
		var storyText = inkStory.Continue().TrimEnd();
		
		if (inkStory.variablesState.HasVariable("angered"))
		{
			angered = (int) inkStory.variablesState["angered"] == 1;
		}
		else
		{
			angered = false;
		}
		
		if (inkStory.variablesState.HasVariable("ordered_drink"))
		{
			orderedDrink = (int) inkStory.variablesState["ordered_drink"] == 1;
		}
		else
		{
			orderedDrink = false;
		}
		
		if (inkStory.variablesState.HasVariable("busted"))
		{
			busted = (int) inkStory.variablesState["busted"] == 1;
		}
		
		talkLineText.text = storyText.TrimEnd();
		talkLineText.color = angered ? Color.red : (isHappy ? Color.green : Color.black);
			
		LeanTween.alpha(talkLineText.rectTransform, 1f, 0.4f);
		yield return new WaitForSecondsRealtime(0.2f);
	}

	public IEnumerator ContinueStory(bool isStart = false)
	{
		LeanTween.alphaCanvas(optionsPanel, 0f, 0.3f);
		yield return ShowTitleText(isStart);

		if (inkStory.currentChoices.Count > 0)
		{
			canChooseOptions = true;
			ResetOptions();
			for (int i = 0; i < choiceLineTexts.Length; i++)
			{
				if (i < inkStory.currentChoices.Count)
				{
					choiceLineButtons[i].gameObject.SetActive(true);
					choiceLineTexts[i].text = "(" + (i + 1) + ") : " + inkStory.currentChoices[i].text;
				}
				else
				{
					choiceLineButtons[i].gameObject.SetActive(false);
				}
				responseLineText.gameObject.SetActive(false);
			}
		}
		else
		{
			canChooseOptions = false;
			for (int i = 0; i < choiceLineTexts.Length; i++)
			{
				choiceLineButtons[i].gameObject.SetActive(false);
			}

			responseLineText.text = "[END DIALOG]";
			CanBeClosed = true;
			responseLineText.gameObject.SetActive(true);
			
			if (busted)
			{
				GameManager.I.Busted();
			}
			else if (angered)
			{
				currentDrinker.DoAngry();
			}
			else if (isHappy)
			{
				currentDrinker.DoHappy();
			}
			else
			{
				currentDrinker.NodHead(orderedDrink);
				if (orderedDrink)
				{
					currentDrinker.CurrentState = Drinker.State.WaitingForDrink;
				}
			}
		}
		
		LeanTween.alphaCanvas(optionsPanel, 1f, 0.6f);
	}
}

public static class Utils
{
	
	private static System.Random rng = new System.Random();  

	public static void Shuffle<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}

	public static T GetRandom<T>(this List<T> list)
	{
		return list[Random.Range(0, list.Count - 1)];
	}
}
