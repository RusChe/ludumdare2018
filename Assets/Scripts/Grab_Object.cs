﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab_Object : MonoBehaviour {

    public Camera cam;

    public GameObject GrabObject()
    {
        GameObject ret = null;
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            ret = hit.transform.gameObject;
            print(ret.name);
        }
        return ret;
    }
}
