﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkerManager : MonoBehaviour
{

	private static DrinkerManager instance;

	public static DrinkerManager I
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<DrinkerManager>();
			return instance;
		}
	}
	
	public List<Drinker> drinkers;

	private void Start()
	{
		NewGame();
	}

	public void NewGame()
	{
		var copIndex = Random.Range(0, drinkers.Count - 1);
		for (int i = 0; i < drinkers.Count; i++)
		{
			drinkers[i].Initialize(i == copIndex);
		}
	}
}
