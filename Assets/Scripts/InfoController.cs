﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoController : MonoBehaviour
{

	private static InfoController instance;

	public static InfoController I
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<InfoController>();
			return instance;
		}
	}

	public CanvasGroup infoPanel;
	public Text infoText;

	private IEnumerator showCourotine;

	public void ShowMessage(string msg)
	{
		if (showCourotine != null)
		{
			StopCoroutine(showCourotine);
		}

		showCourotine = ShowMessageC(msg);
		StartCoroutine(showCourotine);
	}

	private IEnumerator ShowMessageC(string msg, float delay = 0.4f, float duration = 2f)
	{
		infoText.text = msg;
		LeanTween.alphaCanvas(infoPanel, 1f, delay);
		
		yield return new WaitForSecondsRealtime(duration);
		
		LeanTween.alphaCanvas(infoPanel, 0f, delay);
	}
}
