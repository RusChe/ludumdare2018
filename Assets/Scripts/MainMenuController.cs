﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{

	public GameObject storyPanel;

	public List<Text> storyTexts;
	
	private Color invisible = new Color(1f, 1f, 1f, 0f);

	private int showStoryIndex = -1;

	private void Start()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		storyPanel.SetActive(false);
		for (int i = 0; i < storyTexts.Count; i++)
		{
			storyTexts[i].color = invisible;
		}
	}

	public void StartGame()
	{
		storyPanel.SetActive(true);
		showStoryIndex = 0;
		ShowNextStory();
	}

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit();
			return;
		}
		if (Input.anyKeyDown)
		{
			ShowNextStory();
		}
	}

	private void ShowNextStory()
	{
		if (showStoryIndex == -1)
			return;
		
		if (showStoryIndex > 0)
		{
			LeanTween.colorText(storyTexts[showStoryIndex - 1].rectTransform, invisible, 0.4f);
		}
		LeanTween.colorText(storyTexts[showStoryIndex].rectTransform, Color.white, 0.4f);
		
		if (showStoryIndex == storyTexts.Count - 1)
		{
			StartCoroutine(DelayLoadScene());
			showStoryIndex = -1;
			return;
		}
		
		showStoryIndex++;
	}

	private IEnumerator DelayLoadScene()
	{
		yield return new WaitForSecondsRealtime(0.4f);
		SceneManager.LoadScene("Scene_1");
	}

}
