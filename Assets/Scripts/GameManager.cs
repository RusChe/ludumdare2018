﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	
	private static GameManager instance;

	public static GameManager I
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<GameManager>();
			return instance;
		}
	}

	[HideInInspector]
	public int Money;
	
	
	[HideInInspector]
	public int Reputation;

	public GameObject moneyPanel;
	public Text moneyText;
	
	public GameObject reputationPanel;
	public Text reputationText;

	public CanvasGroup endPanel;
	public Text endText;

	public CanvasGroup tutorialPanel;
	public List<CanvasGroup> tutorialInfoPanels;
	public int tutorialInfoIndex;

	public bool IsTutorial;

	private bool canRestartGame;

	private void Start()
	{
		NewGame();
	}

	public void NewGame()
	{
		Reputation = 5;
		Money = 0;
		IsTutorial = false;
	}

	private void Update()
	{
		moneyText.text = Money + "$";
		reputationText.text = Reputation.ToString();

		if (canRestartGame && Input.anyKeyDown)
		{
			SceneManager.LoadScene("MainScene");
			return;
		}

		if (!IsTutorial && Input.GetKeyDown(KeyCode.H))
		{
			IsTutorial = true;
			for (int i = 0; i < tutorialInfoPanels.Count; i++)
			{
				tutorialInfoPanels[i].alpha = 0f;
			}
			tutorialInfoIndex = 0;
			LeanTween.alphaCanvas(tutorialInfoPanels[tutorialInfoIndex], 1f, 0.4f);
			LeanTween.alphaCanvas(tutorialPanel, 1f, 0.4f);
		} 
		else if (IsTutorial && Input.anyKeyDown)
		{
			if (tutorialInfoIndex == tutorialInfoPanels.Count - 1)
			{
				IsTutorial = false;
				LeanTween.alphaCanvas(tutorialPanel, 0f, 0.4f);
			}
			else
			{
				LeanTween.alphaCanvas(tutorialInfoPanels[tutorialInfoIndex], 0f, 0.4f);
				tutorialInfoIndex++;
				LeanTween.alphaCanvas(tutorialInfoPanels[tutorialInfoIndex], 1f, 0.4f);
			}
		}

		if (Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	public void IncreaseMoney(int increase)
	{
		StartCoroutine(AnimateIncreaseMoney(increase));
	}

	private IEnumerator AnimateIncreaseMoney(int increase)
	{
		yield return new WaitForSecondsRealtime(0.5f);
		
		LeanTween.colorText(moneyText.rectTransform, Color.green, 0.4f);
		LeanTween.scale(moneyText.gameObject, 2f * Vector3.one, 0.4f);
		
		yield return new WaitForSecondsRealtime(0.3f);
		
		Money += increase;
		
		yield return new WaitForSecondsRealtime(0.6f);
		
		LeanTween.colorText(moneyText.rectTransform, Color.white, 0.4f);
		LeanTween.scale(moneyText.gameObject, Vector3.one, 0.4f);
	}
	
	public void DecreaseReputation(int decrease)
	{
		StartCoroutine(AnimateDecreaseReputation(decrease));
	}

	private IEnumerator AnimateDecreaseReputation(int decrease)
	{
		yield return new WaitForSecondsRealtime(0.5f);
		
		LeanTween.colorText(reputationText.rectTransform, Color.red, 0.4f);
		LeanTween.scale(reputationText.gameObject, 2f * Vector3.one, 0.4f);
		
		yield return new WaitForSecondsRealtime(0.3f);
		
		Reputation -= decrease;
		
		yield return new WaitForSecondsRealtime(0.6f);
		
		LeanTween.colorText(reputationText.rectTransform, Color.white, 0.4f);
		LeanTween.scale(reputationText.gameObject, Vector3.one, 0.4f);

		if (Reputation == 0)
		{
			yield return AnimateEndScreen(false);
		}
	}

	public void Busted()
	{
		StartCoroutine(AnimateEndScreen(true));
	}

	private IEnumerator AnimateEndScreen(bool isBusted)
	{
		FindObjectOfType<InventoryController>().DisableInvetory();
		FindObjectOfType<DialogController>().HideDialog();
		endText.text = isBusted 
			? "You have been sent to rot in jail for serving illegal drinks!\nAt least, you earned " + Money + "$ on top of it.\nPRESS ANY KEY TO CONTINUE"
			: "You have lost all of your reputation and had to close down the pab.\nYou earned " + Money + "$ while it was good.\nPRESS ANY KEY TO CONTINUE";

		LeanTween.alphaCanvas(endPanel, 1f, 0.8f);
		
		yield return new WaitForSecondsRealtime(1.2f);

		canRestartGame = true;
	}
	
}
