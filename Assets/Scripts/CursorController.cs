﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorController : MonoBehaviour
{

	public Image cursorImage;
	public LayerMask interactableMask;

	public Color interactableColor;
	public float interactableScale = 2f;

	private void Update()
	{
		if (GameManager.I.IsTutorial)
			return;
		
		Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2f, Screen.height / 2f));
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 100f, interactableMask))
		{
			if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Surfice"))
			{
				SetCursorSize(false);
			}
			else
			{
				SetCursorSize(true);
			}
		}
		else
		{
			SetCursorSize(false);
		}
	}

	private void SetCursorSize(bool isBig)
	{
		cursorImage.rectTransform.localScale = Vector3.one * (isBig ? interactableScale : 1f);
		cursorImage.color = isBig ? interactableColor : Color.white;
	}

	public void SetCursor(bool isActive)
	{
		cursorImage.gameObject.SetActive(isActive);
	}
}
